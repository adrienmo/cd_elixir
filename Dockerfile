FROM elixir:1.4.4
COPY _build/prod/rel/cd_elixir/ /root
CMD ["/root/bin/cd_elixir", "foreground"]
