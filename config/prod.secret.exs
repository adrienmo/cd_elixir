use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or you later on).
config :cd_elixir, CdElixir.Endpoint,
  secret_key_base: "K3kqi0DWI7L1d7/BJ3KDJv4f1S5heSJQyp2FHg7XKt/EFrHNWR8aLDiqBKuO0uGR"
