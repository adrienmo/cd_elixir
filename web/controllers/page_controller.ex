defmodule CdElixir.PageController do
  use CdElixir.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
